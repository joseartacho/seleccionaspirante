package com.slashmobility.seleccion.joseantoniogonzalezartacho.data;

import java.util.List;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Artacho on 21/06/14.
 */

public class Json {
    @SerializedName("args")
    private Args mArgsJson;

    @SerializedName("headers")
    private Headers mHeadersJson;

    @SerializedName("origin")
    private String mOrigin;

    @SerializedName("url")
    private String mUrl;

    public class Args {
    }

    public class Headers {
        @SerializedName("Accept")
        private String mAccept;

        @SerializedName("Accept-Encoding")
        private String mAcceptEncoding;

        @SerializedName("Accept-Language")
        private String mAcceptLanguage;

        @SerializedName("Connection")
        private String mConnection;

        @SerializedName("Host")
        private String mHost;

        @SerializedName("User-Agent")
        private String mUserAgent;

        @SerializedName("X-Request-Id")
        private String mXRequestId;

        public String getAccept() {
            return mAccept;
        }

        public String getAcceptEncoding() {
            return mAcceptEncoding;
        }

        public String getAcceptLanguage() {
            return mAcceptLanguage;
        }

        public String getConnection() {
            return mConnection;
        }

        public String getHost() {
            return mHost;
        }

        public String getUserAgent() {
            return mUserAgent;
        }

        public String getXRequestId() {
            return mXRequestId;
        }

    }

    public String getOrigin() {
        return mOrigin;
    }

    public String getUrl() {
        return mUrl;
    }


}
