package com.slashmobility.seleccion.joseantoniogonzalezartacho.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.slashmobility.seleccion.joseantoniogonzalezartacho.R;
import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by Artacho on 20/06/14.
 */
public class ListFragment extends Fragment {

    private Button mAddButton;
    private Button mRemoveButton;
    private Button mSortButton;
    private EditText mAddEditText;
    private TextView mEmptyList;
    private ListView mListViewNumbers;

    private int mAddedNumber;
    private ArrayAdapter<Integer> mArrayAdapter;
    private ArrayList<Integer> mArrayList;


    private static final String TAG = "ListFragment";

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list,
                container, false);

        mAddButton = (Button) view.findViewById(R.id.add_button);
        mRemoveButton = (Button) view.findViewById(R.id.remove_button);
        mSortButton = (Button) view.findViewById(R.id.sort_button);
        mAddEditText = (EditText) view.findViewById(R.id.add_edit_text);
        mEmptyList = (TextView) view.findViewById(R.id.empty_list_text_view);
        mListViewNumbers = (ListView) view.findViewById(R.id.list_view_numbers);

        mArrayList = new ArrayList<Integer>();

        mAddButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, mAddEditText.getText().toString());
                if (!mAddEditText.getText().toString().matches("")) {
                mAddedNumber = Integer.parseInt(mAddEditText.getText().toString());
                if (mAddedNumber >= 0) {
                    if (mArrayList.isEmpty()){
                        mEmptyList.setVisibility(View.GONE);
                   }
                    mArrayList.add(mAddedNumber);
                    mAddEditText.getText().clear();
                    //Hide Keyboard
                    InputMethodManager inputManager = (InputMethodManager)
                            getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(),
                            InputMethodManager.HIDE_NOT_ALWAYS);
                    mArrayAdapter = new ArrayAdapter<Integer>(getActivity(), R.layout.item_number_list, mArrayList);
                    mListViewNumbers.setAdapter(mArrayAdapter);

                }
                }
            }
        });

        mRemoveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mArrayList.clear();
                mArrayAdapter = new ArrayAdapter<Integer>(getActivity(), R.layout.item_number_list, mArrayList);
                mListViewNumbers.setAdapter(mArrayAdapter);
                mEmptyList.setVisibility(View.VISIBLE);
            }
        });

        mSortButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Collections.sort(mArrayList);
                mArrayAdapter = new ArrayAdapter<Integer>(getActivity(), R.layout.item_number_list, mArrayList);
                mListViewNumbers.setAdapter(mArrayAdapter);
            }
        });





     return view;
    }
}