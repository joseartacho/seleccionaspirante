package com.slashmobility.seleccion.joseantoniogonzalezartacho.fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.slashmobility.seleccion.joseantoniogonzalezartacho.R;
import com.slashmobility.seleccion.joseantoniogonzalezartacho.activities.ListActivity;
import com.slashmobility.seleccion.joseantoniogonzalezartacho.data.Json;
import com.slashmobility.seleccion.joseantoniogonzalezartacho.preferences.MyPreferences;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.reflect.Type;

/**
 * Created by Artacho on 20/06/14.
 */
public class MainFragment extends Fragment {

    private Button mCallButton;
    private Button mGoToListButton;
    private TextView mResponse;
    private TextView mReversed;

    private static String URL = "http://httpbin.org/get";
    private static final String TAG = "MainFragment";
    private static final String TAG_ORIGIN = "origin";

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main,
                container, false);

        mCallButton = (Button) view.findViewById(R.id.call_service);
        mGoToListButton = (Button) view.findViewById(R.id.go_to_list);
        mResponse = (TextView) view.findViewById(R.id.response_text_view);
        mReversed = (TextView) view.findViewById(R.id.reversed_text_view);

        mCallButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JSONParser jParser = new JSONParser();
                jParser.execute(URL);
            }
        });
        mGoToListButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), ListActivity.class);
                startActivity(intent);
            }
        });

     return view;
    }

    public class JSONParser extends AsyncTask<String, String, String>{

        private ProgressDialog progressDialog = new ProgressDialog(getActivity());
        @Override
        protected void onPreExecute() {
            progressDialog.setMessage("Loading...");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... uri) {
            HttpClient httpclient = new DefaultHttpClient();
            HttpResponse response;
            String responseString = null;
            try {
                response = httpclient.execute(new HttpGet(uri[0]));
                StatusLine statusLine = response.getStatusLine();
                if(statusLine.getStatusCode() == HttpStatus.SC_OK){
                    ByteArrayOutputStream out = new ByteArrayOutputStream();
                    response.getEntity().writeTo(out);
                    out.close();
                    responseString = out.toString();
                } else{
                    //Closes the connection.
                    response.getEntity().getContent().close();
                    throw new IOException(statusLine.getReasonPhrase());
                }
            } catch (ClientProtocolException e) {
                //TODO Handle problems..
            } catch (IOException e) {
                //TODO Handle problems..
            }
            return responseString;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            progressDialog.dismiss();

            final MyPreferences myPreferences =
                    new MyPreferences(getActivity());
            myPreferences.setJson(result);
            Type myTypeToken = new TypeToken<Json>() {}.getType();

            Gson gson = new Gson();
            Json json = gson
                    .fromJson(myPreferences.getJson(),
                            myTypeToken);
            mResponse.setText(json.getOrigin());
            mReversed.setText(new StringBuilder(json.getOrigin()).reverse().toString());

        }
    }
}