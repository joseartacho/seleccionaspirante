package com.slashmobility.seleccion.joseantoniogonzalezartacho.preferences;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Artacho on 21/06/14.
 */
public class MyPreferences {

    private static final String TAG = "MyPreferences";
    private static final String KEY_PREFERENCES = "myPreferences";
    private static final String KEY_JSON = "json";

    private SharedPreferences mSharedPreferences;

    public MyPreferences(Context context) {
        mSharedPreferences = context.
                getSharedPreferences(KEY_PREFERENCES, Context.MODE_PRIVATE);
    }

    public String getJson() {
        return mSharedPreferences.getString(KEY_JSON, null);
    }

    public void setJson(String jsonName) {
        mSharedPreferences.edit().putString(KEY_JSON, jsonName).commit();
    }
}
